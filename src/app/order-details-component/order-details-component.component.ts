import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Book} from '../shared/book';
import {ActivatedRoute, Router} from "@angular/router";
import {BookStoreService} from "../shared/book-store.service";
import {Order} from "../shared/order";
import {Image} from "../shared/image";
import {Orderposition} from "../shared/orderposition";

@Component({
  selector: 'bs-order-details-component',
  templateUrl: './order-details-component.component.html',
  styles: []
})
export class OrderDetailsComponentComponent implements OnInit {

    @Output() showListEvent = new EventEmitter<any>();

    books = [];
    order: Order;
    images: Image[];
    orderposition: Orderposition[];


    constructor(
        private bs : BookStoreService,
        private router: Router,
        private route : ActivatedRoute
    ){}


  ngOnInit() {
        const params = this.route.snapshot.params;
        console.log(params);
     this.bs.getOrder(params["id"]).subscribe(res=> {
         this.order = res;
         //this.getImages(this.order.id);
     });
      this.bs.getBooksofOrder(params["id"]).subscribe(res=> {
          this.books = res;
          console.log(this.books);
      });
      this.bs.getOrderpositionsofOrder(params["id"]).subscribe(res=> {
          this.orderposition = res;
      });

  }
}
