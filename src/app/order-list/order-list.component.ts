import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../shared/order";
import {Book} from "../shared/book";
import {AuthService} from "../shared/authentication.service";
import {BookStoreService} from "../shared/book-store.service";

@Component({
  selector: 'div.bs-order-list',
  templateUrl: './order-list.component.html',
  styles: []
})
export class OrderListComponent implements OnInit {

  @Input() order: Order;
  orders: Order[];
  constructor(private authService: AuthService, private bs : BookStoreService ) { }

  ngOnInit() {
      this.bs.getOrders(this.authService.getCurrentUserId()).subscribe(res=>this.orders=res)
  }

}
