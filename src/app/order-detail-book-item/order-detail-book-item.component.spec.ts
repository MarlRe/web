import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDetailBookItemComponent } from './order-detail-book-item.component';

describe('OrderDetailBookItemComponent', () => {
  let component: OrderDetailBookItemComponent;
  let fixture: ComponentFixture<OrderDetailBookItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDetailBookItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailBookItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
