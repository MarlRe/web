import {Component, Input, OnInit} from '@angular/core';
import {Book} from "../shared/book";
import {Orderposition} from "../shared/orderposition";
import {Image} from "../shared/image"

@Component({
  selector: 'a.bs-order-detail-book-item',
  templateUrl: './order-detail-book-item.component.html',
  styles: []
})
export class OrderDetailBookItemComponent implements OnInit {

  @Input() book: Book;
  @Input() position: Orderposition;
  @Input() image: Image;
  constructor() { }

  ngOnInit() {
  }

}
