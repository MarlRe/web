import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookRateComponent } from './book-rate.component';

describe('BookRateComponent', () => {
  let component: BookRateComponent;
  let fixture: ComponentFixture<BookRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
