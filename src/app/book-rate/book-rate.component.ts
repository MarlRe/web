import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, FormArray, Validators} from "@angular/forms";
import {BookFactory} from "../shared/book-factory";
import {BookStoreService} from "../shared/book-store.service";
import {Book} from "../shared/book";
import {Review} from "../shared/review";
import {AuthService} from "../shared/authentication.service";
import {BookRateErrorMessages} from "./book-rate-error-messages";
//import arrayContaining = jasmine.arrayContaining;

@Component({
  selector: 'bs-book-rate',
  templateUrl: './book-rate.component.html',
  styles: []
})
export class BookRateComponent implements OnInit {

    myForm: FormGroup;
    book=BookFactory.empty();
    errors: {[key:string]: string } = {};

    constructor(private fb: FormBuilder, private bs : BookStoreService, private route:ActivatedRoute,
                private router:Router, public authService : AuthService) {

    }


    ngOnInit() {
        //isbn
        //const isbn = this.route.snapshot.params['isbn'];
        //aktuelles Buch wird ausgewählt
        //this.bs.getSingle(isbn).subscribe(b => {
          //  this.book = b;
        //});
        this.initReview();
    }

    //Bewertung initialisieren
    initReview(){
        //Form Model anlegen
        this.myForm = this.fb.group ({
            rating: ['', Validators.required],
            description: ['', Validators.required]
        });
        this.myForm.statusChanges.subscribe(()=>this.updateErrorMessages());
    }

    updateErrorMessages() {
        this.errors = {};
        for (const message of BookRateErrorMessages) {
            const control = this.myForm.get(message.forControl);
            if (control &&
                control.dirty &&
                control.invalid &&
                control.errors[message.forValidator] &&
                !this.errors[message.forControl]) {
                this.errors[message.forControl] = message.text;
            }
        }
    }

    submitForm(){
        console.log(this.route);
        const isbn = this.route.snapshot.params['isbn'];
        const user_id = this.authService.getCurrentUserId();
        const review = new Review(null,this.myForm.value.description,this.myForm.value.rating, user_id);
        this.bs.saveReview(review, isbn).subscribe(res => {
                this.router.navigate(['../../books', isbn],{
                    relativeTo: this.route
                });
            })
        }



}
