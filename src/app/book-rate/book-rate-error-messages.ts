export class ErrorMessage {
    constructor(
        public forControl: string,
        public forValidator: string,
        public text: string
    ) { }
}
export const BookRateErrorMessages = [
    new ErrorMessage('rating', 'required', 'Es muss ein Rating angegeben werden'),
    //new ErrorMessage('isbn', 'minlength', 'Die ISBN muss mindestens 10 Zeichen enthalten'),
    //new ErrorMessage('isbn', 'maxlength', 'Eine ISBN darf höchstens 13 Zeichen haben'),
    //new ErrorMessage('published', 'required', 'Es muss ein Erscheinungsdatum angegeben werden'),
    new ErrorMessage('description', 'required', 'Es muss eine Beschreibung angegeben werden'),
    //new ErrorMessage('price', 'required', 'Es muss ein price angegeben werden')
];