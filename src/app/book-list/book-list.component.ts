import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Author, Image, Review, Book} from '../shared/book';
import {BookStoreService} from "../shared/book-store.service";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'bs-book-list',
  templateUrl: './book-list.component.html',
  styles: []
})
export class BookListComponent implements OnInit {

  books: Book[];
  @Output() showDetailsEvent = new EventEmitter<Book>();

  constructor(private bs : BookStoreService) { }

  ngOnInit(){
      this.bs.getAll().subscribe(res=>{
        this.books=res;
        console.log(this.books);
      });
  }
    /*showDetails(book:Book){
      this.showDetailsEvent.emit(book);
      console.log(book);
    }*/

}
