import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Book} from '../shared/book';
import {ActivatedRoute, Router} from "@angular/router";
import {BookStoreService} from "../shared/book-store.service";
import {BookFactory} from "../shared/book-factory";
import {AuthService} from "../shared/authentication.service";
import {Order} from "../shared/order";


@Component({
  selector: 'bs-book-details',
  templateUrl: './book-details.component.html',
  styles: []
})
export class BookDetailsComponent implements OnInit {

  @Output() showListEvent = new EventEmitter<any>();

  book: Book = BookFactory.empty();


  constructor(
    private bs : BookStoreService,
    private router: Router,
    private route : ActivatedRoute,
    public authService : AuthService
  ){}



  ngOnInit() {
    const params = this.route.snapshot.params;
    //this.book = this.bs.getSingle(params['isbn']);
    this.bs.getSingle(params['isbn']).subscribe(b=>this.book=b);
  }

  getRating(num:number){
    return new Array(num);
  }
  /*showBookList(){
    this.showListEvent.emit();
  }*/

  //durchschnittliche Bewertung für dieses Buch berechnen
  getAverageRating(book:Book){
    let x = 0;
    let y = 0;
      for (let review of book.reviews) {
          x+=review['rating'];
          y++;
      }
      if(y!=0)
        return new Array(Math.round(x/y));
      else return;

  }
  removeBook(){
    if(confirm('Buch wirklich löschen?')){
      this.bs.remove(this.book.isbn)
          .subscribe(res => this.router.navigate(['../'], {relativeTo: this.route}));
    }
  }

  saveLocal(){
      let arr2 = [];
      let price = 0;
    if(localStorage.getItem('shoppingcart')){
      let arr = JSON.parse(localStorage.getItem('shoppingcart'));
        //let arr2 = [];
        for (let ar of arr) {
            arr2.push(ar);
        }
      arr2.push(this.book);
      price = parseFloat(localStorage.getItem('shoppingcart_price'));
      price += parseFloat(this.book.price);

      //localStorage.setItem('shoppingcart', JSON.stringify(arr2));
    }
     else {
        arr2.push(this.book);
        price = parseFloat(this.book.price);
    }
    localStorage.setItem('shoppingcart_price', price.toString());
      localStorage.setItem('shoppingcart', JSON.stringify(arr2));
  }


}
