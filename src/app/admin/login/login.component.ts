import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from "../../shared/authentication.service";
import {BookStoreService} from "../../shared/book-store.service";
import {Author, Image, Review, Book} from '../../shared/book';
import {Order} from "../../shared/order";


interface Response {
    response: string;
    result: {
        token: string;
    };
}

@Component({
    selector: 'bs-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    orders: Order[];

    constructor(private fb: FormBuilder, private router: Router,
                private authService: AuthService, private bs : BookStoreService ) { }

    ngOnInit() {
        this.loginForm = this.fb.group({
            username: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
        });

        if(this.isLoggedIn()){
            this.bs.getOrders(this.authService.getCurrentUserId()).subscribe(res=>this.orders=res)
        }
    }

    login() {
        const val = this.loginForm.value;
        if (val.username && val.password) {
            this.authService.login(val.username, val.password).subscribe(res => {
                const resObj = res as Response;
                if (resObj.response === "success") {
                    this.authService.setLocalStorage(resObj.result.token);
                    this.router.navigateByUrl('/');
                }
            });
        }
    }


    isLoggedIn(){
        return this.authService.isLoggedIn();
    }

    logout(){
        this.authService.logout();
    }
}