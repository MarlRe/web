import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BookFormComponent} from "../book-form/book-form.component";
import {Book} from "../shared/book";



const routes: Routes = [
    {path: '', component: BookFormComponent},
    {path: ':isbn', component: BookFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
