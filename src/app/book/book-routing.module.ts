import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BookListComponent} from "../book-list/book-list.component";
import {BookDetailsComponent} from "../book-details/book-details.component";
import {ShoppingCartListComponent} from "../shopping-cart-list/shopping-cart-list.component";

const routes: Routes = [
    {path: '', component: BookListComponent},
    {path:':isbn', component: BookDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookRoutingModule { }
