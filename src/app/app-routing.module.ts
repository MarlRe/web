import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookListComponent} from "./book-list/book-list.component";
import { BookDetailsComponent} from "./book-details/book-details.component";
import { HomeComponent} from "./home/home.component";
import { BookFormComponent } from "./book-form/book-form.component";
import {LoginComponent} from "./admin/login/login.component";
import {BookRateComponent} from "./book-rate/book-rate.component";
import {ShoppingCartListComponent} from "./shopping-cart-list/shopping-cart-list.component";
import {OrderDetailsComponentComponent} from "./order-details-component/order-details-component.component";

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent },
    { path: 'books', component: BookListComponent },
    { path: 'books/:isbn', component: BookDetailsComponent },
    { path: 'admin', component: BookFormComponent},
    { path: 'admin/:isbn', component: BookFormComponent },
    { path: 'login', component: LoginComponent},
    { path: 'rate/:isbn', component: BookRateComponent},
    { path: 'shoppingcart', component: ShoppingCartListComponent},
    { path: 'order/:id', component: OrderDetailsComponentComponent},

];

@NgModule ({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule {}
