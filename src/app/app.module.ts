import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookListItemComponent } from './book-list-item/book-list-item.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookStoreService} from "./shared/book-store.service";
import { HomeComponent } from './home/home.component';
import { AppRoutingModule} from "./app-routing.module"
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { DateValueAccessorModule} from "angular-date-value-accessor";
import {ReactiveFormsModule} from "@angular/forms";
import { BookFormComponent } from './book-form/book-form.component';
import {LoginComponent} from "./admin/login/login.component";
import {AuthService} from "./shared/authentication.service";
import {TokenInterceptorService} from "./shared/token-interceptor.service";
import { BookRateComponent } from './book-rate/book-rate.component';
import { ShoppingCartListComponent } from './shopping-cart-list/shopping-cart-list.component';
import { OrderDetailsComponentComponent } from './order-details-component/order-details-component.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderDetailBookItemComponent } from './order-detail-book-item/order-detail-book-item.component';

@NgModule({
  declarations: [
      AppComponent,
      BookListComponent,
      BookListItemComponent,
      BookDetailsComponent,
      HomeComponent,
      BookFormComponent,
      LoginComponent,
      BookRateComponent,
      ShoppingCartListComponent,
      OrderDetailsComponentComponent,
      OrderListComponent,
      OrderDetailBookItemComponent,
  ],
  imports: [
    BrowserModule, AppRoutingModule, HttpClientModule, ReactiveFormsModule, DateValueAccessorModule
  ],
  providers: [BookStoreService, AuthService,  {

      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
