import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Book} from "../shared/book";
import {AuthService} from "../shared/authentication.service";
import {Review} from "../shared/review";
import {Order} from "../shared/order";
import {ActivatedRoute, Router} from "@angular/router";
import {BookStoreService} from "../shared/book-store.service";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'bs-shopping-cart-list',
  templateUrl: './shopping-cart-list.component.html',
  styles: []
})
export class ShoppingCartListComponent implements OnInit {

  books: Book[];
  price: number = 0;
  @Output() showDetailsEvent = new EventEmitter<Book>();

  constructor(public authService : AuthService, private bs : BookStoreService,
              private router: Router,
              private route : ActivatedRoute) {}

    ngOnInit(){
        if(localStorage.getItem('shoppingcart')) {
            let arr = JSON.parse(localStorage.getItem('shoppingcart'));
            this.books = arr;
        }
        if(localStorage.getItem('shoppingcart_price')) {
            this.price = parseFloat(localStorage.getItem('shoppingcart_price'));
        }
        /*if(this.books){
            for(let book of this.books){
                let x = book.price;
                this.price += x;
            }
        }*/
  }

    orderBooks(){
        //console.log(this.route);
        //const isbn = this.route.snapshot.params['isbn'];
        const user_id = this.authService.getCurrentUserId();
        const order = new Order(null,user_id,new Date(), this.books);
        this.bs.saveOrder(order).subscribe(res => {
             localStorage.removeItem('shoppingcart')
             this.router.navigate(['../login'],{
             relativeTo: this.route
            });
        })
    }



}
