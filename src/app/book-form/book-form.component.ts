import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, FormArray, Validators} from "@angular/forms";
import {BookFactory} from "../shared/book-factory";
import {BookStoreService} from "../shared/book-store.service";
import {Book} from "../shared/book";
import {BookFormErrorMessages} from "./book-form-error-messages";
import {AuthService} from "../shared/authentication.service";
import {Author} from "../shared/author";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'bs-book-form',
  templateUrl: './book-form.component.html',
  styles: []
})
export class BookFormComponent implements OnInit {
  myForm: FormGroup;
  book=BookFactory.empty();
  errors: {[key:string]: string } = {};
  isUpdating = false;
  thumbnails:FormArray;
  Allauthors:Author[];
  //authors: FormArray;

  constructor(private fb: FormBuilder, private bs : BookStoreService, private route:ActivatedRoute,
              private router:Router, public authService : AuthService) {

  }

  ngOnInit() {
    const isbn = this.route.snapshot.params['isbn'];
    if(isbn){
      this.isUpdating=true;
      this.bs.getSingle(isbn).subscribe(b => {
          this.book = b;
          this.initBook();
      });
        this.bs.getAuthors().subscribe(b =>{
            this.Allauthors = b;
            //this.FilterAuthors();
        });

    }
    else{
        this.bs.getAuthors().subscribe(b =>{
            this.Allauthors = b;
        });
    }
      //this.Allauthors = this.bs.getAuthors();
      //this.FilterAuthors();
      this.initBook();
  }

  initBook(){
    this.buildThumbnailsArray();
   // this.buildAuthorsArray();
    //dient als placeholder wenn upgedated wird
        this.myForm = this.fb.group ({
            id: this.book.id,
            title: [this.book.title, Validators.required],
            price: [this.book.price, Validators.required],
            subtitle: this.book.subtitle,
            isbn: [this.book.isbn, [Validators.required]],
            description: this.book.description,
            thumbnails: this.thumbnails,
            published: new Date(this.book.published),
            //authors: this.authors
    });

    this.myForm.statusChanges.subscribe(() => this.updateErrorMessages());
   // this.buildOthersArray();
  }

  buildThumbnailsArray(){

    this.thumbnails = this.fb.array(
        this.book.images.map(
          t => this.fb.group({
              id: this.fb.control(t.id),
              url: this.fb.control(t.url),
              title: this.fb.control(t.title)
          })
        ))
  }

  removeAuthor($id){
      this.FilterAuthors();
      for(let x of this.book.authors){
        if(x.id == $id){
            this.Allauthors.push(x);
        }
    }
    this.book.authors = this.book.authors.filter(item => this.filterByID(item, this.Allauthors));
  }
  addAuthor($id){
      this.FilterAuthors();
      for(let x of this.Allauthors){
          if(x.id == $id) {
              this.book.authors.push(x);
          }
      }
      this.FilterAuthors();
  }

    FilterAuthors(){
        this.Allauthors =  this.Allauthors.filter(item => this.filterByID(item, this.book.authors));
    }
    buildOthersArray($id){
        if(this.isUpdating) {
            //let missing = this.Allauthors.filter(item => this.filterByID(item, this.book.authors));
            for (let x of this.book.authors) {
                if ($id == x.id) {
                    return false;
                }
            }
            return true;
        }
        else
            return true;
    }


    filterByID(item, book) {
        for (let y of book) {
            if (item.id == y.id) {
                return false;
            }
        }
        return true;

    }

  addThumbnailControl(){
      this.thumbnails.push(this.fb.group({
          url: null,
          title: null
      }))
  }


  submitForm(){
      this.myForm.value.thumbnails =
          this.myForm.value.thumbnails.filter(thumbnail => thumbnail.url);
      //this.myForm.value.authors =
        //  this.myForm.value.authors.filter(author => author.id);
      const book:Book = BookFactory.fromObject(this.myForm.value);
      //just copy rating and authors
      let netto_price = (parseFloat(this.myForm.value.price)/110*100);
      book.netto_price = (Math.round(netto_price*100)/100).toString();
      book.rating = this.book.rating;
      //book.authors =  this.myForm.value.authors;
      book.authors = this.book.authors;
      //update book
      if(this.isUpdating){
          this.bs.update(book).subscribe(res => {
              this.router.navigate(['../../books', book.isbn],{
                  relativeTo: this.route
              })
              this.FilterAuthors();
          })
      }
      else{
          //new book
          book.user_id = this.authService.getCurrentUserId();
          console.log(book);
          this.bs.create(book).subscribe(res => {
              this.book = BookFactory.empty();
              this.myForm.reset(BookFactory.empty());
          })
      }
  }

    updateErrorMessages() {
        this.errors = {};
        for (const message of BookFormErrorMessages) {
            const control = this.myForm.get(message.forControl);
            if (control &&
                control.dirty &&
                control.invalid &&
                control.errors[message.forValidator] &&
                !this.errors[message.forControl]) {
                this.errors[message.forControl] = message.text;
            }
        }
    }

}
