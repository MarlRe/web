export class ErrorMessage {
    constructor(
        public forControl: string,
        public forValidator: string,
        public text: string
    ) { }
}
export const BookFormErrorMessages = [
    new ErrorMessage('title', 'required', 'Ein Buchtitel muss angegeben werden'),
    new ErrorMessage('isbn', 'required', 'Es muss eine ISBN angegeben werden'),
    new ErrorMessage('isbn', 'minlength', 'Die ISBN muss mindestens 5 Zeichen enthalten'),
    new ErrorMessage('isbn', 'maxlength', 'Eine ISBN darf höchstens 30 Zeichen haben'),
    new ErrorMessage('published', 'required', 'Es muss ein Erscheinungsdatum angegeben werden'),
    new ErrorMessage('price', 'required', 'Es muss ein Preis angegeben werden'),
    new ErrorMessage('price', 'pattern', 'Es muss eine Zahl angegeben werden! Kommazahlen mit .'),
    new ErrorMessage('description', 'required', 'Es muss eine Beschreibung angegeben werden')
];
