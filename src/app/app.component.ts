import { Component } from '@angular/core';
import {Book} from "./shared/book";
import  {AuthService} from "./shared/authentication.service";

@Component({
  selector: 'bs-root',
  //template: '<bs-book-list *ngIf="listOn" (showDetailsEvent)="showDetails($event)"></bs-book-list><bs-book-details *ngIf="detailsOn" [book]="book" (showListEvent)="showList()"></bs-book-details>',
  templateUrl: './app.component.html',
    styles: []
})
export class AppComponent {

  listOn = true;
  detailsOn = false;

  book:Book;

  constructor (private authService:AuthService){}

  showDetails (book:Book){
    this.book = book;
    this.listOn = false;
    this.detailsOn = true;
  }

  showList (){
    this.listOn = true;
    this.detailsOn = false;
  }

  isLoggedIn(){
    return this.authService.isLoggedIn();
  }

  isAdmin(){
    return this.authService.isAdmin();
  }

  getLoginLabel(){
    if(this.isLoggedIn()){
      return "Konto";
    }
    else
      return "Login";
  }

}
