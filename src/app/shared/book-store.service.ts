import { Injectable } from '@angular/core';
import {Book, Author, Image, Review} from './book';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Order} from "./order";

@Injectable()
export class BookStoreService {

    private api='http://bookstore-rest.s1510456031.student.kwmhgb.at/api';

  //books: Book[];
  constructor(private http:HttpClient) {

  }

  getAll() : Observable<Array<Book>>{
    return this.http.get(`${this.api}/books`)
        .retry(3).catch(this.errorHandler);
  }

  getSingle(isbn: string) : Observable<Book> {
      return this.http.get(`${this.api}/book/${isbn}`)
          .retry(3).catch(this.errorHandler);
  }

  remove (isbn: string):Observable<any>{
      return this.http.delete(`${this.api}/book/${isbn}`)
          .catch(this.errorHandler);
  }

  create (book:Book) : Observable<Book>{
      return this.http.post(`${this.api}/book`, book)
          .catch(this.errorHandler);
  }

  update (book:Book):Observable<any>{
      console.log(book.isbn);
      return this.http.put(`${this.api}/book/${book.isbn}`, book)
          .catch(this.errorHandler);
  }

  saveReview (review:Review, isbn:string):Observable<any>{
      return this.http.post(`${this.api}/rate/${isbn}`, review)
          .catch(this.errorHandler);
  }

    saveOrder (order:Order):Observable<any>{
        return this.http.post(`${this.api}/shoppingcart/order`, order)
            .catch(this.errorHandler);
    }

    getOrders (id: number):Observable<any>{
        return this.http.get(`${this.api}/orders/${id}`)
            .catch(this.errorHandler);
    }

    getOrder (id: number):Observable<any>{
      return this.http.get(`${this.api}/order/${id}`)
          .retry(3).catch(this.errorHandler);
    }

    getAuthors ():Observable<Array<Author>>{
        return this.http.get(`${this.api}/authors`)
            .catch(this.errorHandler);
    }
   /* getAuthorswithoutAuthorsofBook (isbn: string):Observable<Array<Author>>{
        return this.http.get(`${this.api}/authors/${isbn}`)
            .catch(this.errorHandler);
    }*/

    //get all Images of a book
    getBooksofOrder (id: number):Observable<Array<any>>{
        return this.http.get(`${this.api}/BooksofOrder/${id}`)
            .catch(this.errorHandler);
    }

    getOrderpositionsofOrder (id: number):Observable<Array<any>>{
        return this.http.get(`${this.api}/order/position/${id}`)
            .catch(this.errorHandler);
    }


  private errorHandler(error:Error):Observable<any>{
      return Observable.throw(error);
  }
}
