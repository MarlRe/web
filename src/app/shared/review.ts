export class Review {
    constructor (public id: number,
                 public description: string,
                 public rating: number,
                 public user_id: number
    ){}
}
