import { TestBed, inject } from '@angular/core/testing';

import { TokenInterseceptorService } from './token-interceptor.service';

describe('TokenInterseceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenInterseceptorService]
    });
  });

  it('should be created', inject([TokenInterseceptorService], (service: TokenInterseceptorService) => {
    expect(service).toBeTruthy();
  }));
});
