import {Book} from './book';
import {Review} from "./review";
export {Book} from './book';

export class Order {
    constructor (public id: number,
                 public user_id: number,
                 public ordered: Date,
                 public books: Book[],
                 public total_price?: string,
                 public netto_price?: string,
                 public brutto_price?: string
    ){}
}
