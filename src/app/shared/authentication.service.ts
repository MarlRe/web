import {Injectable} from '@angular/core';
import {isNullOrUndefined} from "util";
import {HttpClient} from "@angular/common/http";
import * as decode from 'jwt-decode';

//npm install --save-dev jwt-decode


interface User {
    result: {
        created_at: Date,
        email: string,
        id: number,
        name: string,
        updated_at: Date,
        usertype: number
    }
}

@Injectable()
export class AuthService {

    private api:string = 'http://bookstore-rest.s1510456031.student.kwmhgb.at/api/auth';//'http://localhost:8080/api/auth';

    constructor(private http: HttpClient) {
    }

    login(email: string, password: string ) {
        return this.http.post(`${this.api}/login`, {'email': email, 'password': password});
    }

    public setCurrentUserId(){
        this.http.get<User>(`${this.api}/user`).retry(3).subscribe(res =>{
                localStorage.setItem('userId', res.result.id.toString());
            }
        );
    }

    public getCurrentUserId(){
        return Number.parseInt(localStorage.getItem('userId'));
    }

    public setLocalStorage(token: string) {
        console.log("Storing token");
        console.log(token);
        const decodedToken = decode(token);
        console.log(decodedToken);
        console.log(decodedToken.user.id);
        console.log(decodedToken.user.usertype);
        localStorage.setItem('usertype', decodedToken.user.usertype);
        localStorage.setItem('token', token);
        localStorage.setItem('userId', decodedToken.user.id);
    }
    public isAdmin(){
        return Number.parseInt(localStorage.getItem('usertype')) == 1;
    }

    logout() {
        this.http.post(`${this.api}/logout`, {});
        localStorage.removeItem("token");
        localStorage.removeItem("userId");
        localStorage.removeItem('usertype');
        console.log("logged out");
    }

    public isLoggedIn() {
        return !isNullOrUndefined(localStorage.getItem("token"));
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    }



}