import {Book} from "./book";

export class Orderposition {
    constructor (public id: number,
                 public book_id: number,
                 public order_id: number,
                 public amount: number,
                 public netto_price: number,
                 public brutto_price: number
    ){}
}
